import pdb
import tempfile
import os

import vtk
import math
import numpy as np
from scipy import sparse
import scipy.sparse.linalg as linalg_sp
from scipy.sparse import vstack, hstack, coo_matrix, csc_matrix
import skimage.draw as skdraw
import pyvista as pv
import pyacvd
import SimpleITK as sitk
import itertools
# import seedselector
from vtk.util.numpy_support import vtk_to_numpy


###     Input/Output    ###
def readvtk(filename):
    """Read VTK file"""
    reader = vtk.vtkPolyDataReader()
    reader.SetFileName(filename)
    reader.Update()
    return pv.wrap(reader.GetOutput())


def writevtk(surface, filename, type='ascii'):
    """Write binary or ascii VTK file"""
    writer = vtk.vtkPolyDataWriter()
    if vtk.vtkVersion.GetVTKMajorVersion() > 5:
        writer.SetInputData(surface)
    else:
        writer.SetInput(surface)
    writer.SetFileName(filename)
    if type == 'ascii':
        writer.SetFileTypeToASCII()
    elif type == 'binary':
        writer.SetFileTypeToBinary()
    writer.Write()


def get_centroid_sitk(mask_img, as_physical=False):
    """
    """
    _filt = sitk.LabelShapeStatisticsImageFilter()
    _filt.Execute(mask_img)
    if as_physical:
        return _filt.GetCentroid(1)
    else:
        return mask_img.TransformPhysicalPointToIndex(_filt.GetCentroid(1))

def get_bbox(img, 
    label_val=1, 
    dilate=0,
    padding=None,
    ):
    """
    """
    if dilate: 
        img = sitk.BinaryDilate(img, dilate)

    labelfilter = sitk.LabelStatisticsImageFilter()
    labelfilter.Execute(img, img)
    bbox = labelfilter.GetBoundingBox(label_val)

    min_ = [int(bbox[2*i]) for i in range(3)]
    max_ = [int(bbox[2*i+1]+1) for i in range(3)] # bbox are stored as (x_min, x_max, ...)

    if padding is None: 
        padding = [0, 0, 0]

    if not isinstance(padding, (list, tuple)):
        padding = [padding] * 3
    
    img_size = img.GetSize()
    padding_=[int(p/2) for p in padding]
    min_ = [min_[i]-padding_[i] if min_[i]-padding_[i] >= 0 else 0 for i in range(3)]
    max_ = [max_[i]+padding_[i] if max_[i]+padding_[i] <= img_size[i] else img_size[i] for i in range(3)]
    
    bbox = list(itertools.chain(*zip(min_, max_))) # repackage bbox
    return bbox

def link_scalar_value(polydata, 
        scalar_img,
        scalar_name,
        mask_img=None,
        link=None):
    """
    """
    _scalar_img = sitk.GetArrayFromImage(scalar_img)
    _scalar_img[np.isnan(_scalar_img)] = 0
    _scalar_img = sitk.GetImageFromArray(_scalar_img)
    _scalar_img.CopyInformation(_scalar_img)

    if link is None:
        link = get_link(mask_img=mask_img, 
            mesh=polydata)
    
    scalar_array = [_scalar_img[index] for index in link]
    try:
        polydata[scalar_name] = scalar_array
    except: 
        polydata = pv.wrap(polydata)
        polydata[scalar_name] = scalar_array
    return polydata, link


def z_rotation(vector,theta):
    """Rotates 3-D vector around z-axis"""
    R = np.array([[np.cos(theta), -np.sin(theta),0],[np.sin(theta), np.cos(theta),0],[0,0,1]])
    return np.dot(R,vector)


def clear_array(polydata):
    """ Clean polydata
    """
    n_point_arrays = polydata.GetPointData().GetNumberOfArrays()
    n_cell_arrays = polydata.GetCellData().GetNumberOfArrays()
    for nth_point in range(n_point_arrays): 
        polydata.GetPointData().RemoveArray(nth_point)

    for nth_cell in range(n_cell_arrays): 
        polydata.GetCellData().RemoveArray(nth_cell)
    return polydata


def clean_polydata(polydata, tolerance=0):
    """
    """
    cleaner = vtk.vtkCleanPolyData()
    cleaner.SetDebug(1)
    cleaner.SetTolerance(tolerance)
    cleaner.SetInputData(polydata)
    cleaner.Update()
    polydata = cleaner.GetOutput()
    return polydata


def get_vtkPointLocator(polydata):
    """
    """
    locator = vtk.vtkPointLocator()
    locator.SetDataSet(polydata)
    locator.BuildLocator()
    return locator


def uniform_meshing(polydata, n_clusters=1000):
    """
    """
    wrapped_data = pv.wrap(polydata)
    clus = pyacvd.Clustering(wrapped_data)
    # clus.subdivide(3)
    clus.cluster(n_clusters)
    polydata = clus.create_mesh()
    return polydata


def threshold_polydata(polydata, 
        lower=None, 
        upper=None, 
        all_scalars=True):
    threshold_filt = vtk.vtkThreshold()
    threshold_filt.SetInputData(polydata)

    if upper is not None and lower is not None:
        threshold_filt.ThresholdBetween(lower, upper)
    elif lower is not None:
        threshold_filt.ThresholdByUpper(lower)
    elif upper is not None:
        threshold_filt.ThresholdByUpper(upper)

    threshold_filt.SetAllScalars(all_scalars)
    threshold_filt.Update()
    return threshold_filt.GetOutput()


def remove_polydata(main_mesh, substract_mesh):
    """
    """
    distances_filt = vtk.vtkDistancePolyDataFilter()
    distances_filt.SetInputData(0, main_mesh)
    distances_filt.SetInputData(1, substract_mesh)
    distances_filt.Update()
    dist_mesh = distances_filt.GetOutput()
    
    # apply threshold
    threshold_filt = vtk.vtkThreshold()
    threshold_filt.SetInputData(dist_mesh)
    threshold_filt.ThresholdByUpper(1)
    threshold_filt.Update()
    output = threshold_filt.GetOutput()
    
    # convert to polydata
    output = to_vtkPolyData(output)
    output = clear_array(output)
    return output
    
def get_neighbor(index):
    """
    """
    x, y, z = index
    return [
            [x-1, y-1, z-1], 
            [x, y-1, z-1], 
            [x+1, y-1, z-1], 
            [x-1, y, z-1], 
            [x, y, z-1], 
            [x+1, y, z-1], 
            [x-1, y+1, z-1], 
            [x, y+1, z-1], 
            [x+1, y+1, z-1], 

            [x-1, y-1, z], 
            [x, y-1, z], 
            [x+1, y-1, z], 
            [x-1, y, z], 
            [x+1, y, z], 
            [x-1, y+1, z], 
            [x, y+1, z], 
            [x+1, y+1, z], 

            [x-1, y-1, z+1], 
            [x, y-1, z+1], 
            [x+1, y-1, z+1], 
            [x-1, y, z+1], 
            [x, y, z+1], 
            [x+1, y, z+1], 
            [x-1, y+1, z+1], 
            [x, y+1, z+1], 
            [x+1, y+1, z+1], 
        ]

def get_euclidean_dist(point_a, point_b):
    """
    """
    return np.sum((np.array(point_a) - np.array(point_b))**2)**0.5


def find_closest(pos, mask_img):
    """
    """
    # get index
    index = mask_img.TransformPhysicalPointToIndex(pos)
    
    if mask_img[index]:
        return index

    def get_val(index, img, c=0):
        try: 
            return img[index] 
        except: 
            return c
    neighbor_index = get_neighbor(index)
    neighbor_val = [get_val(_index, mask_img) for _index in neighbor_index]

    valid_index = [index for index, val in zip(neighbor_index, neighbor_val) if val]
    if len(valid_index) == 0:
        return None

    elif len(valid_index) == 1:
        return valid_index[0]

    else:
        cont_index = mask_img.TransformPhysicalPointToContinuousIndex(pos)
        neighbor_dist = [get_euclidean_dist(cont_index, _index) for _index in valid_index]
        return valid_index[np.argmin(neighbor_dist)]


def get_link(mask_img, 
        mesh): 
    """
    """
    n_points = mesh.GetNumberOfPoints()
    link = np.zeros((n_points, 3)) * np.nan
    missing = []
    locator = vtk.vtkPointLocator()
    points =  vtk.vtkPoints()
    locator.InitPointInsertion(points, mesh.GetBounds())
    locator_index = []
    
    for point_id in range(n_points):
        pos = mesh.GetPoint(point_id)
        index = find_closest(pos=pos, 
                mask_img=mask_img)

        if index is not None:
            link[point_id] = index
            locator.InsertNextPoint(pos)
            locator_index.append(index)

        else: 
            missing.append([pos, point_id])

    for pos, point_id in missing: 
        closest_id = locator.FindClosestInsertedPoint(pos)
        index = locator_index[closest_id]

        locator.InsertNextPoint(pos)
        locator_index.append(index)

        link[point_id] = index
    return link.astype(int).tolist()

def marching_cubes(
    img,
    decimate=0.7,
    smoothing_iterations=30,
    relaxation_factor=0.2,
    closing_radius=0,
    tmp_file=None,
    rm_tmpfile=True,
    n_clusters=10000,
    tolerance=0,
    clean=True,
    tetra=True,
):
    """
    Creates a mesh from a binary mask.

    Quite a mess right now, because returns a volumetric pyvista mesh if tetra=True
    and a surface cardiac_utils.mesh.SpatialData otherwise.
    Still useful because VTK ignores the "direction" information from imaging
    data.

    Maybe someday I'll find the courage to clean that without breaking everything
    else.

    :param decimate: VTK marching cubes parameter
    :param smoothing_iterations: VTK marching cubes parameter
    :param relaxation_factor: VTK marching cubes parameter
    :param closing_radius: Apply binary closing before meshing
    :param tmp_file:
    :param rm_tmpfile:
    :param n_clusters: Number of points in the mesh
    :param clean: VTK marching cubes parameter
    :param tetra: True for a volumetric, False for a surface mesh
    :return:
    """
    try:
        import pyacvd
        import vtk
        from vtk.util.numpy_support import numpy_to_vtk, vtk_to_numpy
        import SimpleITK as sitk
        #try: 
        from tetgen import TetGen

        #except: 
        #    tetra = False

    except: 
        raise ModuleNotFoundError(f"Cannot import necessary modules to perform image meshing.")

    mask = img
    img_dim = img.GetDimension()

    if closing_radius:
        # mask = closing(mask, selem=ball(closing_radius))
        if not isinstance(closing_radius, (list, tuple)):
            closing_radius = [closing_radius] * img_dim
        mask = sitk.BinaryMorphologicalClosing(mask, closing_radius)

    if tmp_file is None:
        handle, tmp_file = tempfile.mkstemp(suffix=".mha")
        os.close(handle)

    sitk.WriteImage(mask, tmp_file)

    reader = vtk.vtkMetaImageReader()
    reader.SetFileName(tmp_file)
    reader.Update()
    vtkimg = reader.GetOutput()
    # Let's reset everything here as vtk doesn't
    # apply the full transform correctly.
    vtkimg.SetOrigin((0, 0, 0))
    vtkimg.SetSpacing((1, 1, 1))

    # apply marching cubes
    triangulator = vtk.vtkDiscreteMarchingCubes()
    triangulator.SetInputData(vtkimg)
    triangulator.Update()
    polydata = triangulator.GetOutput()

    # apply transformation
    direction = np.array(mask.GetDirection()).reshape((3, 3)).T
    original_points = vtk_to_numpy(polydata.GetPoints().GetData())
    new_points = ((original_points * img.GetSpacing()[::-1]) @ direction) + img.GetOrigin()[
        ::-1
    ]
    new_vtk_points = vtk.vtkPoints()
    new_vtk_points.SetData(numpy_to_vtk(new_points))
    polydata.SetPoints(new_vtk_points)

    if decimate:
        # decimation
        decimator = vtk.vtkDecimatePro()
        decimator.SetInputData(polydata)
        decimator.SetTargetReduction(decimate)
        decimator.Update()
        polydata = decimator.GetOutput()

    if smoothing_iterations:
        # smoothing
        smoother = vtk.vtkSmoothPolyDataFilter()
        smoother.SetNumberOfIterations(smoothing_iterations)
        smoother.SetRelaxationFactor(relaxation_factor)
        smoother.SetInputData(polydata)
        smoother.Update()
        polydata = smoother.GetOutput()

    if n_clusters is not None:
        polydata = uniform_meshing(polydata, n_clusters=n_clusters)

    if clean:
        polydata = clean_polydata(polydata, tolerance=tolerance)


    if rm_tmpfile:
        os.remove(tmp_file)

    if tetra:
        pvsurf = pv.wrap(polydata)
        tet = TetGen(pvsurf)
        tet.make_manifold()
        tet.tetrahedralize()
        polydata = tet.grid # type(res) == vtkUnstructureGrid

        # convert back to vtkPolydata
        geometrier = vtk.vtkGeometryFilter()
        geometrier.SetInputData(polydata)
        geometrier.Update()
        polydata = geometrier.GetOutput()
        polydata = pv.wrap(polydata)
    return polydata


def to_vtkPolyData(mesh):
    """
    """
    geometrier = vtk.vtkGeometryFilter()
    geometrier.SetInputData(mesh)
    geometrier.Update()
    polydata = geometrier.GetOutput()
    polydata = pv.wrap(polydata)
    return polydata
    

# def append(polydata1, polydata2):
def append(*polydatas):
    """Define new polydata appending polydata1 and polydata2"""
    appender = vtk.vtkAppendPolyData()
    # appender.AddInputData(polydata1)
    # appender.AddInputData(polydata2)
    for polydata in polydatas:
        appender.AddInputData(polydata)
    # appender.AddInputData(polydata2)
    appender.Update()
    return appender.GetOutput()


def get_centroid(polydata):
    """ Apply center of mass filter on mesh.
    """
    filt = vtk.vtkCenterOfMass()
    filt.SetInputData(polydata)
    filt.Update()
    return filt.GetCenter()


def extractboundaryedge(polydata):
    edge = vtk.vtkFeatureEdges()
    if vtk.vtkVersion.GetVTKMajorVersion() > 5:
        edge.SetInputData(polydata)
    else:
        edge.SetInput(polydata)
    edge.FeatureEdgesOff()
    edge.NonManifoldEdgesOff()
    edge.Update()
    return edge.GetOutput()


def find_create_path(mesh, p1, p2):
    """Get shortest path (using Dijkstra algorithm) between p1 and p2 on the mesh. Returns a polydata"""
    dijkstra = vtk.vtkDijkstraGraphGeodesicPath()
    if vtk.vtkVersion().GetVTKMajorVersion() > 5:
        dijkstra.SetInputData(mesh)
    else:
        dijkstra.SetInput(mesh)
    dijkstra.SetStartVertex(p1)
    dijkstra.SetEndVertex(p2)
    dijkstra.Update()
    return dijkstra.GetOutput()


def compute_geodesic_distance(mesh, id_p1, id_p2):
    """Compute geodesic distance from point id_p1 to id_p2 on surface 'mesh'
    It first computes the path across the edges and then the corresponding distance adding up point to point distances)"""
    path = find_create_path(mesh, id_p1, id_p2)
    total_dist = 0
    n = path.GetNumberOfPoints()
    for i in range(n-1):   # Ids are ordered in the new polydata, from 0 to npoints_in_path
        p0 = path.GetPoint(i)
        p1 = path.GetPoint(i+1)
        dist = math.sqrt(math.pow(p0[0]-p1[0], 2) + math.pow(p0[1]-p1[1], 2) + math.pow(p0[2]-p1[2], 2) )
        total_dist = total_dist + dist
    return total_dist, path


def transfer_all_scalar_arrays_by_point_id(m1, m2):
    """ Transfer all scalar arrays from m1 to m2 by point id"""
    for i in range(m1.GetPointData().GetNumberOfArrays()):
        print('Transferring scalar array: {}'.format(m1.GetPointData().GetArray(i).GetName()))
        m2.GetPointData().AddArray(m1.GetPointData().GetArray(i))


def get_ordered_cont_ids_based_on_distance(mesh):
    """ Given a contour, get the ordered list of Ids (not ordered by default).
    Open the mesh duplicating the point with id = 0. Compute distance transform of point 0
    and get a ordered list of points (starting in 0) """
    m = vtk.vtkMath()
    m.RandomSeed(0)
    # copy the original mesh point by point
    points = vtk.vtkPoints()
    polys = vtk.vtkCellArray()
    cover = vtk.vtkPolyData()
    nver = mesh.GetNumberOfPoints()
    points.SetNumberOfPoints(nver+1)

    new_pid = nver  # id of the duplicated point
    added = False
    for j in range(mesh.GetNumberOfCells()):
        # get the 2 point ids
        ptids = mesh.GetCell(j).GetPointIds()
        cell = mesh.GetCell(j)
        if (ptids.GetNumberOfIds() != 2):
            # print "Non contour mesh (lines)"
            break

        # read the 2 involved points
        pid0 = ptids.GetId(0)
        pid1 = ptids.GetId(1)
        p0 = mesh.GetPoint(ptids.GetId(0))   # returns coordinates
        p1 = mesh.GetPoint(ptids.GetId(1))

        if pid0 == 0:
            if added == False:
                # Duplicate point 0. Add gaussian noise to the original point
                new_p = [p0[0] + m.Gaussian(0.0, 0.0005), p0[1] + m.Gaussian(0.0, 0.0005), p0[2] + m.Gaussian(0.0, 0.0005)]
                points.SetPoint(new_pid, new_p)
                points.SetPoint(pid1, p1)
                polys.InsertNextCell(2)
                polys.InsertCellPoint(pid1)
                polys.InsertCellPoint(new_pid)
                added = True
            else:  # act normal
                points.SetPoint(ptids.GetId(0), p0)
                points.SetPoint(ptids.GetId(1), p1)
                polys.InsertNextCell(2)
                polys.InsertCellPoint(cell.GetPointId(0))
                polys.InsertCellPoint(cell.GetPointId(1))
        elif pid1 == 0:
            if added == False:
                new_p = [p1[0] + m.Gaussian(0.0, 0.0005), p1[1] + m.Gaussian(0.0, 0.0005), p1[2] + m.Gaussian(0.0, 0.0005)]
                points.SetPoint(new_pid, new_p)
                points.SetPoint(pid0, p0)
                polys.InsertNextCell(2)
                polys.InsertCellPoint(pid0)
                polys.InsertCellPoint(new_pid)
                added = True
            else:  # act normal
                points.SetPoint(ptids.GetId(0), p0)
                points.SetPoint(ptids.GetId(1), p1)
                polys.InsertNextCell(2)
                polys.InsertCellPoint(cell.GetPointId(0))
                polys.InsertCellPoint(cell.GetPointId(1))

        else:
            points.SetPoint(ptids.GetId(0), p0)
            points.SetPoint(ptids.GetId(1), p1)
            polys.InsertNextCell(2)
            polys.InsertCellPoint(cell.GetPointId(0))
            polys.InsertCellPoint(cell.GetPointId(1))

    if added == False:
        print('Warning: I have not added any point, list of indexes may not be correct.')
    cover.SetPoints(points)
    cover.SetPolys(polys)
    # cover.SetVerts(polys)
    if not vtk.vtkVersion.GetVTKMajorVersion() > 5:
        cover.Update()
    
    # cover=mesh
    # compute distance from point with id 0 to all the rest
    npoints = cover.GetNumberOfPoints()
    dists = np.zeros(npoints)
    for i in range(npoints):
        [dists[i], poly] = compute_geodesic_distance(cover, int(0), i)
    list_ = np.argsort(dists).astype(int)
    return list_[0:len(list_)-1]    # skip last one, duplicated


def ExtractVTKPoints(mesh):
    """Extract points from vtk structures. Return the Nx3 numpy.array of the vertices."""
    n = mesh.GetNumberOfPoints()
    vertex = np.zeros((n, 3))
    for i in range(n):
        mesh.GetPoint(i, vertex[i, :])
    return vertex


def ExtractVTKTriFaces(mesh):
    """Extract triangular faces from vtkPolyData. Return the Nx3 numpy.array of the faces (make sure there are only triangles)."""
    m = mesh.GetNumberOfCells()
    faces = np.zeros((m, 3), dtype=int)
    for i in range(m):
        ptIDs = vtk.vtkIdList()
        mesh.GetCellPoints(i, ptIDs)
        if ptIDs.GetNumberOfIds() != 3:
            raise Exception("Nontriangular cell!")
        faces[i, 0] = ptIDs.GetId(0)
        faces[i, 1] = ptIDs.GetId(1)
        faces[i, 2] = ptIDs.GetId(2)
    return faces


def ComputeLaplacian(vertex, faces):
    """Calculates the laplacian of a mesh
    vertex 3xN numpy.array: vertices
    faces 3xM numpy.array: faces"""
    n = vertex.shape[1]
    m = faces.shape[1]

    # compute mesh weight matrix
    W = sparse.coo_matrix((n, n))
    for i in np.arange(1, 4, 1):
        i1 = np.mod(i - 1, 3)
        i2 = np.mod(i, 3)
        i3 = np.mod(i + 1, 3)
        pp = vertex[:, faces[i2, :]] - vertex[:, faces[i1, :]]
        qq = vertex[:, faces[i3, :]] - vertex[:, faces[i1, :]]
        # normalize the vectors
        pp = pp / np.sqrt(np.sum(pp ** 2, axis=0))
        qq = qq / np.sqrt(np.sum(qq ** 2, axis=0))

        # compute angles
        ang = np.arccos(np.sum(pp * qq, axis=0))
        W = W + sparse.coo_matrix((1 / np.tan(ang), (faces[i2, :], faces[i3, :])), shape=(n, n))
        W = W + sparse.coo_matrix((1 / np.tan(ang), (faces[i3, :], faces[i2, :])), shape=(n, n))

    # compute laplacian
    d = W.sum(axis=0)
    D = sparse.dia_matrix((d, 0), shape=(n, n))
    L = D - W
    return L


def flat_w_constraints(m, boundary_ids, constraints_ids, x0_b, y0_b, x0_c, y0_c):
    """ Conformal flattening fitting boundary points to (x0_b,y0_b) coordinate positions
    and additional contraint points to (x0_c,y0_c).
    Solve minimization problem using quadratic programming: https://en.wikipedia.org/wiki/Quadratic_programming"""
    penalization = 1000
    vertex = ExtractVTKPoints(m).T    # 3 x n_vertices
    faces = ExtractVTKTriFaces(m).T

    n = vertex.shape[1]
    L = ComputeLaplacian(vertex, faces)
    L = L.tolil()
    L[boundary_ids, :] = 0.0     # Not conformal there
    for i in range(boundary_ids.shape[0]):
         L[boundary_ids[i], boundary_ids[i]] = 1
    L = L*penalization

    Rx = np.zeros(n)
    Ry = np.zeros(n)
    Rx[boundary_ids] = x0_b * penalization
    Ry[boundary_ids] = y0_b * penalization

    L = L.tocsr()
    # result = np.zeros((Rx.size, 2))

    nconstraints = constraints_ids.shape[0]
    M = np.zeros([nconstraints, n])   # M, zero rows except 1 in constraint point
    for i in range(nconstraints):
        M[i, constraints_ids[i]] = 1
    dx = x0_c
    dy = y0_c

    block1 = hstack([L.T.dot(L), M.T])

    zeros_m = coo_matrix(np.zeros([len(dx),len(dx)]))
    block2 = hstack([M, zeros_m])

    C = vstack([block1, block2])

    prodx = coo_matrix([L.T.dot(Rx)])
    dxx = coo_matrix([dx])
    cx = hstack([prodx, dxx])

    prody = coo_matrix([L.T.dot(Ry)])
    dyy = coo_matrix([dy])
    cy = hstack([prody, dyy])
    solx = linalg_sp.spsolve(C, cx.T)
    soly = linalg_sp.spsolve(C, cy.T)

    # print('There are: ', len(np.argwhere(np.isnan(solx))), ' nans')
    # print('There are: ', len(np.argwhere(np.isnan(soly))), ' nans')
    if len(np.argwhere(np.isnan(solx))) > 0:
        print('WARNING!!! matrix is singular. It is probably due to the convergence of 2 different division lines in the same point.')
        print('Trying to assign different 2D possition to same 3D point. Try to create new division lines or increase resolution of mesh.')

    pd = vtk.vtkPolyData()
    pts = vtk.vtkPoints()

    pts.SetNumberOfPoints(n)
    for i in range(n):
        pts.SetPoint(i, solx[i], soly[i], 0)

    pd.SetPoints(pts)
    pd.SetPolys(m.GetPolys())
    # pd.SetVerts(m.GetPolys())
    pd.Modified()
    return pd
