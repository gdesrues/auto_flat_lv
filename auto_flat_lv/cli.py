""" Comment Line utilities.
"""
import argparse
import os
import re
import pdb

import numpy as np
import SimpleITK as sitk
import scipy.ndimage as spn
import tqdm 
from tqdm import tqdm as std_tqdm

from . import flat_lv as f_lv
from . import aux_functions as af

LVEPI_ID = ".*ct1.*lvepi.*sax.*"
LVENDO_ID = ".*ct1.*lvendo.*sax.*"
RVEPI_ID = ".*ct1.*rvepi.*sax.*"
THICKNESS_ID = ".*ct1.*thickness.*"
SEED_MESH_ID = ".*seeds.*"
FLAT_MESH_ID = ".*flat_lv.*"
SURFACE_MESH_ID = ".*lvwall_contour_mesh.*"
MV_MESH_ID = ".*mv_mesh.*"

def getargs():
    parser = argparse.ArgumentParser(description="", 
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('inputs',
                action='store',
                nargs='+',
                help="""  """, 
                default=None,
                type=str,
                )

    parser.add_argument('--save_dir', '-s',
                action='store',
                help=""" Output write directory. """, 
                default='', # save in the same folder.
                type=str,
                )

    parser.add_argument('--datadir',
                action='store',
                help=""" """, 
                default='/media/sharedata/DicomHL', # save in the same folder.
                type=str,
                )

    parser.add_argument('--output_shape', '-o',
                action='store',
                help=""" Input lvepi sitk image. """, 
                type=int,
                default=256,
                )

    parser.add_argument('--pad',
                action='store',
                help=""" Input lvepi sitk image. """, 
                type=int,
                default=1,
                )
    
    parser.add_argument('--scalar_name',
                action='store',
                help=""" Input lvepi sitk image. """, 
                type=str,
                default="Thickness",
                )

    parser.add_argument('--lvepi',
                action='store',
                help=""" Input lvepi sitk image. """, 
                type=str,
                )

    parser.add_argument('--lvendo',
                action='store',
                help=""" Input lvendo sitk image. """, 
                type=str,
                )

    parser.add_argument('--rvepi',
                action='store',
                help=""" Input rvepi sitk image. """, 
                type=str,
                )

    parser.add_argument('--thickness',
                action='store',
                help=""" Input thickness sitk image. """, 
                type=str,
                )
    return parser.parse_args()


def cli_to_np():
    """
    """
    args = getargs()
    save_dir = os.path.abspath(os.path.expanduser(args.save_dir))
    os.makedirs(save_dir, exist_ok=True)
    inputs = args.inputs
    
    inputs_iter = std_tqdm(inputs) # add progress bar

    for input_path in inputs_iter: 
        # read mesh
        mesh = af.readvtk(input_path)
        
        mesh_np = fl.get_flat_lv_np(polydata=mesh,
                output_shape=args.output_shape,
                pad=args.pad, 
                scalar_name=args.scalar_name,
                )
        
        # write numpy output
        basename, _ = os.path.splitext(os.path.basename(input_path))
        _save_path = os.path.join(save_dir, f"{basename}.npy")
        np.write(_save_path, mesh_np)


def cli_seed_dbg():
    """
    """
    args = getargs()
    save_dir = os.path.abspath(os.path.expanduser(args.save_dir))
    os.makedirs(save_dir, exist_ok=True)

    # get input images
    if args.inputs is not None: # auto parsing
        input_paths = args.inputs 

        for file in input_paths:
            if re.match(LVEPI_ID, file, re.IGNORECASE):
                lvepi_img_path = file
                lvepi_img = sitk.ReadImage(lvepi_img_path)

            elif re.match(LVENDO_ID, file, re.IGNORECASE):
                lvendo_img_path = file
                lvendo_img = sitk.ReadImage(lvendo_img_path)

            elif re.match(RVEPI_ID, file, re.IGNORECASE):
                rvepi_img_path = file
                rvepi_img = sitk.ReadImage(rvepi_img_path)

            elif re.match(SURFACE_MESH_ID, file, re.IGNORECASE):
                surface_mesh_path = file
                surface_mesh = af.readvtk(surface_mesh_path)

            elif re.match(MV_MESH_ID, file, re.IGNORECASE):
                mv_mesh_path = file
                mv_mesh = af.readvtk(mv_mesh_path)
    lvwall_img = sitk.Xor(lvepi_img, lvendo_img)

    seed_mesh, debug_mesh = f_lv.get_seeds(lvwall_img=lvwall_img, 
            lvwall_contour_mesh=surface_mesh,
            rvepi_img=rvepi_img,
            mv_mesh=mv_mesh,
            aux_angle=10,
            debug=True)
    # write 
    af.writevtk(seed_mesh, "dbg_seed.vtk")
    af.writevtk(debug_mesh, "dbg_debug.vtk")

    
def cli_flat_seed():
    """
    """
    args = getargs()
    save_dir = os.path.abspath(os.path.expanduser(args.save_dir))
    os.makedirs(save_dir, exist_ok=True)

    # get input images
    if args.inputs is not None: # auto parsing
        input_paths = args.inputs 

        for file in input_paths:
            if re.match(SEED_MESH_ID, file, re.IGNORECASE):
                seed_mesh_path = file
                seed_mesh = af.readvtk(seed_mesh_path)

            elif re.match(SURFACE_MESH_ID, file, re.IGNORECASE):
                surface_mesh_path = file
                surface_mesh = af.readvtk(surface_mesh_path)

            elif re.match(FLAT_MESH_ID, file, re.IGNORECASE):
                flat_mesh_path = file
                flat_mesh = af.readvtk(flat_mesh_path)
    # else: # when individual input is provided
    #     seed_mesh = af.readvtk(args.seed)
    #     surface_mesh = af.readvtk(args.surface)
    #     flat_mesh = af.readvtk(args.flat_mesh)

    flat_seed = f_lv.get_flat_seed(seed_mesh=seed_mesh,
            surface_mesh=surface_mesh,
            flat_mesh=flat_mesh,
            )

    # writing output image
    af.writevtk(flat_seed,
        os.path.join(save_dir, 'flat_seed.vtk'))


def cli_remap():
    """
    """
    args = getargs()
    save_dir = os.path.abspath(os.path.expanduser(args.save_dir))
    os.makedirs(save_dir, exist_ok=True)

    if args.inputs is not None: # auto parsing
        input_paths = args.inputs 
        for file in input_paths:
            if re.match(SURFACE_MESH_ID, file, re.IGNORECASE):
                surface_mesh_path = file
                surface_mesh = af.readvtk(surface_mesh_path)

            elif re.match(FLAT_MESH_ID, file, re.IGNORECASE):
                disk_mesh_path = file
                disk_mesh = af.readvtk(disk_mesh_path)

            else: # TODO: Add wider support to load remap array
                remap_array_path = file
                remap_array = np.load(remap_array_path)[0]
                remap_array = spn.zoom(remap_array, 16)

    remap_disk_mesh, remap_surface_mesh = f_lv.remap(array=remap_array,
            disk_mesh=disk_mesh, 
            surface_mesh=surface_mesh,
            array_name='Attention Map',
            )
    af.writevtk(remap_disk_mesh, os.path.join(save_dir, "remap_disk_mesh.vtk"))
    af.writevtk(remap_surface_mesh, os.path.join(save_dir, "remap_surface_mesh.vtk"))


def cli_flat_lv():
    """
    """
    args = getargs()
    save_dir = os.path.abspath(os.path.expanduser(args.save_dir))
    os.makedirs(save_dir, exist_ok=True)

    # get input images
    if args.inputs is not None: # auto parsing
        input_paths = args.inputs 

        for file in input_paths:
            if re.match(LVEPI_ID, file, re.IGNORECASE):
                lvepi_path = file
                lvepi_img = sitk.ReadImage(lvepi_path)

            elif re.match(LVENDO_ID, file, re.IGNORECASE):
                lvendo_path = file
                lvendo_img = sitk.ReadImage(lvendo_path)

            elif re.match(RVEPI_ID, file, re.IGNORECASE):
                rvepi_path = file
                rvepi_img = sitk.ReadImage(rvepi_path)

            elif re.match(THICKNESS_ID, file, re.IGNORECASE):
                thickness_path = file
                thickness_img = sitk.ReadImage(thickness_path)

    else: # when individual input is provided
        lvepi_img = sitk.ReadImage(args.lvepi)
        lvendo_img = sitk.ReadImage(args.lvendo)
        rvepi_img = sitk.ReadImage(args.rvepi)
        thickness_img = sitk.ReadImage(args.thickness)

    output = f_lv.automate(lvepi_img=lvepi_img,
            lvendo_img=lvendo_img,
            rvepi_img=rvepi_img,
            thickness_img=thickness_img,
            output_shape=args.output_shape,
            )
    output['flat_seeds'] = f_lv.get_flat_seed(seed_mesh=output['seeds'],
            surface_mesh=output['lvwall_contour_mesh'],
            flat_mesh=output['flat_lv'],
            )

    # writing output image
    af.writevtk(output['lvwall_contour_mesh'], 
        os.path.join(save_dir, 'lvwall_contour_mesh.vtk'))

    af.writevtk(output['rvepi_mesh'], 
        os.path.join(save_dir, 'rvepi_mesh.vtk'))

    af.writevtk(output['mv_mesh'], 
        os.path.join(save_dir, 'mv_mesh.vtk'))

    af.writevtk(output['seeds'], 
        os.path.join(save_dir, 'seeds.vtk'))

    af.writevtk(output['flat_seeds'], 
        os.path.join(save_dir, 'flat_seeds.vtk'))

    af.writevtk(output['flat_lv'], 
        os.path.join(save_dir, 'flat_lv.vtk'))

    np.save(os.path.join(save_dir, 'flat_lv.npy'),
        output['flat_lv_np'])


def cli_clean():
    """
    """
    args = getargs()

    # get input images
    inputs = args.inputs 
    iterator = std_tqdm(inputs)
    for input_path in iterator:
        for _dir, _, files in os.walk(input_path):
            for file in files:
                if re.match('flat_lv.npy', file, re.IGNORECASE):
                    flat_np_path = os.path.join(_dir, file)
                    flat_np = np.load(flat_np_path)

        if np.isnan(flat_np).sum() > 0: 
            # redo
            datadir = args.datadir
            _input_path = os.path.join(datadir, os.path.basename(input_path))
            for _dir, _, files in os.walk(_input_path):
                for file in files:
                    if re.match(LVEPI_ID, file, re.IGNORECASE):
                        lvepi_path = os.path.join(_dir, file)
                        lvepi_img = sitk.ReadImage(lvepi_path)

                    elif re.match(LVENDO_ID, file, re.IGNORECASE):
                        lvendo_path = os.path.join(_dir, file)
                        lvendo_img = sitk.ReadImage(lvendo_path)

                    elif re.match(RVEPI_ID, file, re.IGNORECASE):
                        rvepi_path = os.path.join(_dir, file)
                        rvepi_img = sitk.ReadImage(rvepi_path)

                    elif re.match(THICKNESS_ID, file, re.IGNORECASE):
                        thickness_path = os.path.join(_dir, file)
                        thickness_img = sitk.ReadImage(thickness_path)

            # run flattening 
            output = f_lv.automate(lvepi_img=lvepi_img,
                    lvendo_img=lvendo_img,
                    rvepi_img=rvepi_img,
                    thickness_img=thickness_img,
                    output_shape=args.output_shape,
                    )

            # writing output image
            _save_dir = input_path
            af.writevtk(output['lvwall_contour_mesh'], 
                os.path.join(_save_dir, 'lvwall_contour_mesh.vtk'))

            af.writevtk(output['rvepi_mesh'], 
                os.path.join(_save_dir, 'rvepi_mesh.vtk'))

            af.writevtk(output['mv_mesh'], 
                os.path.join(_save_dir, 'mv_mesh.vtk'))

            af.writevtk(output['seeds'], 
                os.path.join(_save_dir, 'seeds.vtk'))

            af.writevtk(output['flat_lv'], 
                os.path.join(_save_dir, 'flat_lv.vtk'))

            np.save(os.path.join(_save_dir, 'flat_lv.npy'),
                output['flat_lv_np'])



def cli_packagemake():
    """
    """
    args = getargs()
    save_dir = os.path.abspath(os.path.expanduser(args.save_dir))
    os.makedirs(save_dir, exist_ok=True)

    # get input images
    inputs = args.inputs 
    iterator = std_tqdm(inputs)
    for input_path in iterator:
        for _dir, _, files in os.walk(input_path):
            for file in files:
                if re.match('flat_lv.npy', file, re.IGNORECASE):
                    flat_np_path = os.path.join(_dir, file)
                    flat_np = np.load(flat_np_path)
        # get binary mask
        mask = f_lv.get_flat_mask(output_shape=args.output_shape,)

        # get dirname
        patient_id = os.path.basename(input_path)
        # write as npz
        _save_path = os.path.join(save_dir, f"{patient_id}.npz")
        np.savez_compressed(_save_path, 
                bullseye_map=flat_np,
                bullseye_mask=mask)


def cli_datamake():
    """ 
    """
    args = getargs()
    inputs = args.inputs
    save_dir = os.path.abspath(os.path.expanduser(args.save_dir))
    os.makedirs(save_dir, exist_ok=True)
    iterator = std_tqdm(inputs)
    for input_path in iterator:
        try:
            ## Parse
            for _dir, _, files in os.walk(input_path):
                for file in files:
                    if re.match(LVEPI_ID, file, re.IGNORECASE):
                        lvepi_path = os.path.join(_dir, file)
                        lvepi_img = sitk.ReadImage(lvepi_path)

                    elif re.match(LVENDO_ID, file, re.IGNORECASE):
                        lvendo_path = os.path.join(_dir, file)
                        lvendo_img = sitk.ReadImage(lvendo_path)

                    elif re.match(RVEPI_ID, file, re.IGNORECASE):
                        rvepi_path = os.path.join(_dir, file)
                        rvepi_img = sitk.ReadImage(rvepi_path)

                    elif re.match(THICKNESS_ID, file, re.IGNORECASE):
                        thickness_path = os.path.join(_dir, file)
                        thickness_img = sitk.ReadImage(thickness_path)

            # run flattening 
            output = f_lv.automate(lvepi_img=lvepi_img,
                    lvendo_img=lvendo_img,
                    rvepi_img=rvepi_img,
                    thickness_img=thickness_img,
                    output_shape=args.output_shape,
                    )

            # writing output image
            _save_dir = os.path.join(save_dir, os.path.basename(input_path))
            os.makedirs(_save_dir, exist_ok=True)
            af.writevtk(output['lvwall_contour_mesh'], 
                os.path.join(_save_dir, 'lvwall_contour_mesh.vtk'))

            af.writevtk(output['rvepi_mesh'], 
                os.path.join(_save_dir, 'rvepi_mesh.vtk'))

            af.writevtk(output['mv_mesh'], 
                os.path.join(_save_dir, 'mv_mesh.vtk'))

            af.writevtk(output['seeds'], 
                os.path.join(_save_dir, 'seeds.vtk'))

            af.writevtk(output['flat_lv'], 
                os.path.join(_save_dir, 'flat_lv.vtk'))

            np.save(os.path.join(_save_dir, 'flat_lv.npy'),
                output['flat_lv_np'])

        except:
            # trace error file 
            with open(os.path.join(save_dir, "fail_flat_lv_datamake.txt"), "a") as F:
                F.write(f"{input_path}\n")

