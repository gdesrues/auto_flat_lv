"""
"""
import pdb

import math, argparse, os, sys
import numpy as np
import SimpleITK as sitk 
import skimage.draw as skdraw
import vtk
import vtk.util.numpy_support as nps
import tempfile
import pyvista as pv

from . import aux_functions as af


def automate(lvepi_img, 
        lvendo_img, 
        rvepi_img,
        thickness_img, 
        output_shape=256,
        scalar_name="Thickness",
        ):
    """ Automate from sitk to vtk
    """
    lvwall_img = sitk.Xor(lvepi_img, lvendo_img)

    # get mesh 
    lvwall_contour_mesh, rvepi_mesh, mv_mesh = get_surface_mesh(lvepi_img=lvepi_img, 
            lvendo_img=lvendo_img, 
            rvepi_img=rvepi_img)

    # link threshold val
    lvwall_contour_mesh, _ = af.link_scalar_value(lvwall_contour_mesh, 
            scalar_img=thickness_img,
            scalar_name=scalar_name,
            mask_img=lvwall_img,)

    # get seeds 
    seeds = get_seeds(lvwall_img=lvwall_img, 
        rvepi_img=rvepi_img,
        lvwall_contour_mesh=lvwall_contour_mesh,
        mv_mesh=mv_mesh,)

    # clean mesh
    lvwall_contour_mesh = af.clean_polydata(lvwall_contour_mesh)

    # get flat_lv
    flat_lv = get_flat_lv(surface_lv=lvwall_contour_mesh, 
        seeds_poly=seeds)

    # get flat_lv as numpy
    flat_lv_np = get_flat_lv_np(polydata=flat_lv, 
            output_shape=output_shape,
            scalar_name=scalar_name,
            )
    
    return {
        "lvwall_contour_mesh": lvwall_contour_mesh,
        "rvepi_mesh": rvepi_mesh,
        "mv_mesh": mv_mesh,
        "seeds": seeds,
        "flat_lv": flat_lv,
        "flat_lv_np": flat_lv_np,
    }


def get_surface_mesh(lvepi_img, 
        lvendo_img,
        rvepi_img,
    ):
    """ Get required meshes from sitk inputs. 
    """
    lvwall_img = sitk.Xor(lvepi_img, lvendo_img)
    lvepi_contour = sitk.Xor(sitk.BinaryErode(lvepi_img, [1,1,1]), lvepi_img)

    mv_img = sitk.And(lvepi_contour, lvendo_img)
    mv_mesh = af.marching_cubes(mv_img, 
            n_clusters=1000, 
            )

    lvwall_contour = sitk.Xor(mv_img, lvepi_contour)


    lvepi_mesh = af.marching_cubes(lvepi_img, 
            n_clusters=10000, 
            closing_radius=5
            )

    rvepi_mesh = af.marching_cubes(rvepi_img, 
            n_clusters=10000, 
            closing_radius=5
            )

    lvwall_contour_mesh = af.remove_polydata(main_mesh=lvepi_mesh, 
            substract_mesh=mv_mesh)

    return lvwall_contour_mesh, rvepi_mesh, mv_mesh


def get_seeds(lvwall_img,
        rvepi_img,
        lvwall_contour_mesh, 
        mv_mesh,
        debug=False,
        aux_angle=15,
        ):
    """ Automate 3 seeds selection.
    """
    if debug: 
        debug_output = {}

    lv_locator = af.get_vtkPointLocator(lvwall_contour_mesh)
    mv_locator = af.get_vtkPointLocator(mv_mesh)

    bbox = af.get_bbox(lvwall_img) # bbox: [min_x, max_x, min_y, max_y, min_z, max_z]
    az = bbox[-1] - 1
    # ax, ay, _ = af.get_centroid_sitk(lvwall_img[:, :, az-2:az])
    ax, ay, _ = af.get_centroid_sitk(lvwall_img[:, :, az-3:az])
    az -= _

    apex_index = [ax, ay, az]
    apex_pos = lvwall_img.TransformIndexToPhysicalPoint([ax, ay, az])
    if debug: 
        debug_output['apex_centeroid'] = apex_pos
    apex_id = lv_locator.FindClosestPoint(apex_pos)
    apex_pos = lvwall_contour_mesh.GetPoint(apex_id)
    
    rv_centroid = af.get_centroid_sitk(rvepi_img, as_physical=True)
    mv_centroid = af.get_centroid(mv_mesh)
    if debug: 
        debug_output['rv_centroid'] = rv_centroid
        debug_output['mv_centroid'] = mv_centroid
    mv_centroid_index = lvwall_img.TransformPhysicalPointToIndex(mv_centroid)

    theta_zero = lvwall_contour_mesh.GetPoint(lv_locator.FindClosestPoint(rv_centroid))
    lvrv_zero_id = lvwall_contour_mesh.GetPoints().InsertNextPoint(theta_zero)

    theta_zero = mv_mesh.GetPoint(mv_locator.FindClosestPoint(theta_zero))
    if debug: 
        debug_output['mv_theta_zero'] = theta_zero
    theta_zero_id = lv_locator.FindClosestPoint(theta_zero)
    theta_zero_index = lvwall_img.TransformPhysicalPointToIndex(theta_zero)

    # get apex base direction
    ap_base_vec = np.array(mv_centroid_index) - np.array(apex_index)
    rotation_mult = 1
    if ap_base_vec[-1] < 0: 
        rotation_mult = -1
    print(f"Rotation multiplier: {rotation_mult}")
    direct_vec = np.array(theta_zero_index) - np.array(mv_centroid_index)
    rotate_vec = af.z_rotation(direct_vec, rotation_mult * (np.pi/180)*(aux_angle))

    theta_aux_index = np.array(mv_centroid_index) + rotate_vec # /get_euclidean_distance(direct_vec)
    theta_aux_pos = lvwall_img.TransformContinuousIndexToPhysicalPoint(theta_aux_index.tolist())
    theta_aux_mv_id = mv_locator.FindClosestPoint(theta_aux_pos)
    theta_aux_mv_pos = mv_mesh.GetPoint(theta_aux_mv_id)
    if debug: 
        debug_output['mv_theta_ax'] = theta_aux_mv_pos
    theta_aux_id = lv_locator.FindClosestPoint(theta_aux_mv_pos)

    labels = [0, 1, 2]
    nseeds=3
    ids = [apex_id, theta_zero_id, theta_aux_id]
    newpoints = vtk.vtkPoints()
    newvertices = vtk.vtkCellArray()
    labels_array = vtk.vtkDoubleArray()
    labels_array.SetName('seed_label')
    for s in range(nseeds):
        label = labels[s]
        point = lvwall_contour_mesh.GetPoint(ids[s])
        pid = newpoints.InsertNextPoint(point)
        labels_array.InsertNextValue(label)
        # Create the topology of the point (vertex)
        newvertices.InsertNextCell(1)
        newvertices.InsertCellPoint(pid)

    seeds = vtk.vtkPolyData()
    seeds.SetPoints(newpoints)
    seeds.SetVerts(newvertices)
    seeds.GetPointData().AddArray(labels_array)

    if debug: 
        # dbg_point_list = []
        labels_arrays = {key: None for key in debug_output.keys()}
        labels_arrays['main'] = vtk.vtkDoubleArray()
        labels_arrays['main'].SetName('main')
        dbg_newpoints = vtk.vtkPoints()
        dbg_newvertices = vtk.vtkCellArray()
        for ind, (key, val) in enumerate(debug_output.items()):
            if labels_arrays[key] is None:
                labels_arrays[key] = vtk.vtkDoubleArray()
                labels_arrays[key].SetName(key)
            labels_arrays[key].InsertNextValue(1)
            labels_arrays['main'].InsertNextValue(ind+1)
            
            # for s in range(nseeds):
                # label = labels[s]
            # point = lvwall_contour_mesh.GetPoint(ids[s])
            pid = dbg_newpoints.InsertNextPoint(val)
                # Create the topology of the point (vertex)
            dbg_newvertices.InsertNextCell(1)
            dbg_newvertices.InsertCellPoint(pid)

            # add empty array
            for _key in debug_output.keys():
                if _key != key: 
                    if labels_arrays[_key] is None:
                        labels_arrays[_key] = vtk.vtkDoubleArray()
                        labels_arrays[_key].SetName(_key)
                    # labels_array[_key] = vtk.vtkDoubleArray()
                    # _labels_array.SetName(_key)
                    labels_arrays[_key].InsertNextValue(0)
                    # _dbg_point.GetPointData().AddArray(_labels_array)
            # dbg_point_list.append(_dbg_point)

        dbg_output = vtk.vtkPolyData()
        dbg_output.SetPoints(dbg_newpoints)
        dbg_output.SetVerts(dbg_newvertices)
        # dbg_output.GetPointData().AddArray(labels_arrays)
        for val in labels_arrays.values():
            dbg_output.GetPointData().AddArray(val)
        return seeds, dbg_output
    return seeds


def get_flat_lv(surface_lv,
        seeds_poly,
        n = 1/3.0,
        ):
    """ Main flatten function.
    """
    # define disk parameters (external radio, apex and aorta point position)
    rdisk = 1.0
    ap_x0 = np.array([0.0])
    ap_y0 = np.array([0.0])
    
    # Find corresponding seeds in the surface mesh
    locator = vtk.vtkPointLocator()
    locator.SetDataSet(surface_lv)
    locator.BuildLocator()
    id_ap = int(locator.FindClosestPoint(seeds_poly.GetPoint(0)))
    
    # Detect edges -> base contour
    cont = af.extractboundaryedge(surface_lv)
    edge_cont_ids = af.get_ordered_cont_ids_based_on_distance(cont).astype(int)
    
    # find corresponding ordered points in the COMPLETE mesh. Use same locator as before
    cont_base_ids = np.zeros(edge_cont_ids.shape[0]) - 1
    for i in range(cont_base_ids.shape[0]):
        p = cont.GetPoint(edge_cont_ids[i])
        cont_base_ids[i] = locator.FindClosestPoint(p)
    
    # find closest point in base contour to 'Aorta' seed
    locator_base = vtk.vtkPointLocator()
    locator_base.SetDataSet(cont)
    locator_base.BuildLocator()
    id_0 = locator_base.FindClosestPoint(seeds_poly.GetPoint(1))
    id_aux = locator_base.FindClosestPoint(seeds_poly.GetPoint(2))
    # in surface
    id_base0 = locator.FindClosestPoint(cont.GetPoint(id_0))
    id_base_aux = locator.FindClosestPoint(cont.GetPoint(id_aux))
    
    # base -> external disk
    # order cont_base_ids to start in 'aorta'
    ref_base = int(locator.FindClosestPoint(surface_lv.GetPoint(id_base0)))
    reordered_base_cont = np.append(cont_base_ids[int(np.where(cont_base_ids == ref_base)[0]): cont_base_ids.shape[0]],
                                  cont_base_ids[0: int(np.where(cont_base_ids == ref_base)[0])]).astype(int)
    # print('Reference point in base is ', ref_base)
    
    # check if the list of ordered points corresponding to the base contours has to be flipped
    pos_auxpoint = int(np.where(reordered_base_cont == id_base_aux)[0])
    if pos_auxpoint > 20:     # 20 points far... empirical, it will depend on the mesh elements :(
        # Flip
        print('I ll flip the base ids')
        aux = np.zeros(reordered_base_cont.size)
        for i in range(reordered_base_cont.size):
            aux[reordered_base_cont.size - 1 - i] = reordered_base_cont[i]
        reordered_base_cont = np.append(aux[aux.size - 1], aux[0:aux.size - 1]).astype(int)
    
    complete_circumf_t = np.linspace(np.pi, np.pi + 2*np.pi, len(reordered_base_cont), endpoint=False)  # starting in pi
    x0_ext = np.cos(complete_circumf_t) * rdisk
    y0_ext = np.sin(complete_circumf_t) * rdisk
    
    m_disk = af.flat_w_constraints(surface_lv, reordered_base_cont, np.array([id_ap]), x0_ext, y0_ext, ap_x0.astype(float),
                                ap_y0.astype(float))
    af.transfer_all_scalar_arrays_by_point_id(surface_lv, m_disk)
    # writevtk(m_disk, os.path.join(fileroot, filenameroot + '_flat.vtk'))
    
    # Apply Bruno's radial displacement to enlarge central part and get more uniform mesh
    # Paun, Bruno, et al. "Patient independent representation of the detailed cardiac ventricular anatomy."
    # Medical image analysis (2017)
    npoints = m_disk.GetNumberOfPoints()
    m_out = vtk.vtkPolyData()
    points = vtk.vtkPoints()
    
    points.SetNumberOfPoints(npoints)
    for i in range(npoints):
        q = np.array(m_disk.GetPoint(i))
        p = np.copy(q)
        x = math.pow(math.pow(p[0], 2) + math.pow(p[1], 2), np.divide(n, 2.0))*math.cos(math.atan2(p[1], p[0]))
        y = math.pow(math.pow(p[0], 2) + math.pow(p[1], 2), np.divide(n, 2.0))*math.sin(math.atan2(p[1], p[0]))
        points.SetPoint(i, x, y, 0)
    m_out.SetPoints(points)
    m_out.SetPolys(m_disk.GetPolys())
    
    af.transfer_all_scalar_arrays_by_point_id(m_disk, m_out)
    return m_out


def get_flat_mask(output_shape,
        pad=1,):
    """
    """
    center = output_shape//2
    radius = center - pad
    shape = (output_shape, output_shape)
    rr, cc = skdraw.disk((center, center), 
            radius=radius, 
            shape=shape)
    blank = np.zeros(shape)

    for x, y in zip(rr, cc):
        blank[x, y] = 1
    return blank


def get_flat_lv_np(polydata, 
        output_shape,
        pad=1, 
        scalar_name="Thickness", 
        ):
    """ Convert flat_lv mesh to numpy array.
    """
    # get circle mask
    center = output_shape//2
    radius = center - pad
    shape = (output_shape, output_shape)
    rr, cc = skdraw.disk((center, center), 
            radius=radius, 
            shape=shape)
    blank = np.zeros(shape)
    rr_norm = (rr - center)/radius
    cc_norm = (cc - center)/radius

    # set up probe filter
    probe_points = vtk.vtkPoints()
    for r, c in zip(rr_norm, cc_norm):
        probe_points.InsertNextPoint((r, c, 0))
    probe_polydata = vtk.vtkPolyData()
    probe_polydata.SetPoints(probe_points)
    probe_polydata.Modified()

    probe_filt = vtk.vtkProbeFilter()
    probe_filt.SetInputData(probe_polydata)
    probe_filt.SetSourceData(polydata)
    probe_filt.Update()
    output = probe_filt.GetOutput()
    val_array = pv.wrap(output)[scalar_name]
    for x, y, val in zip(rr, cc, val_array):
        blank[x, y] = val
    return blank


def get_flat_seed(seed_mesh,
        surface_mesh, 
        flat_mesh,
        ):
    point_locator = af.get_vtkPointLocator(surface_mesh)
    newpoints = vtk.vtkPoints()
    newvertices = vtk.vtkCellArray()
    labels_array = vtk.vtkDoubleArray()
    labels_array.SetName("seed_array")
    for n_seed in range(seed_mesh.GetNumberOfPoints()):
        # get seed postion 
        seed_pos = seed_mesh.GetPoint(n_seed)

        # project on lv mesh
        _id = point_locator.FindClosestPoint(seed_pos)
        
        # get position on  
        pos = flat_mesh.GetPoint(_id)
        pid = newpoints.InsertNextPoint(pos)
        labels_array.InsertNextValue(n_seed)
        # Create the topology of the point (vertex)
        newvertices.InsertNextCell(1)
        newvertices.InsertCellPoint(pid)
    seed_flat = vtk.vtkPolyData()
    seed_flat.SetPoints(newpoints)
    seed_flat.SetVerts(newvertices)
    seed_flat.GetPointData().AddArray(labels_array)
    return seed_flat

def remap(
        array,
        disk_mesh, 
        surface_mesh, 
        array_name='remap',
        array_pad=1,
        ):
    """ Remap numpy array to disk_mesh.
    """
    shape = array.shape[0]
    center = shape//2
    radius = center - array_pad

    n_points = disk_mesh.GetNumberOfPoints()
    remap_array = []
    for ind in range(n_points): 
        # get pos 
        x_pos, y_pos, _ = disk_mesh.GetPoint(ind)
        
        # get x, y in index
        x = int((x_pos+1)*radius)
        y = int((y_pos+1)*radius)
        remap_array.append(array[x, y])
    disk_mesh = pv.wrap(disk_mesh)  
    surface_mesh = pv.wrap(surface_mesh)

    disk_mesh[array_name] = remap_array
    surface_mesh[array_name] = remap_array
    return disk_mesh, surface_mesh


def remap_dict(
        array_dict,
        disk_mesh, 
        surface_mesh, 
        array_pad=1,
        ):
    """ Remap numpy array to disk_mesh.
    """

    disk_mesh = pv.wrap(disk_mesh)  
    surface_mesh = pv.wrap(surface_mesh)
    n_points = disk_mesh.GetNumberOfPoints()

    for key, array in array_dict.items():
        shape = array.shape[0]
        center = shape//2
        radius = center - array_pad

        remap_array = []
        for ind in range(n_points): 
            # get pos 
            x_pos, y_pos, _ = disk_mesh.GetPoint(ind)
            
            # get x, y in index
            x = int((x_pos+1)*radius)
            y = int((y_pos+1)*radius)
            remap_array.append(array[x, y])

        disk_mesh[key] = remap_array
        surface_mesh[key] = remap_array
    return disk_mesh, surface_mesh
