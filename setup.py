from setuptools import setup


setup(
    name='auto_flat_lv',
    version='0.0.1',
    author='Buntheng LY',
    packages=["auto_flat_lv"],
    #long_description=long_description,
    #long_description_content_type="text/markdown",
    include_package_data=True,
    classifiers=[
        "Programming Language :: Python :: 3.6",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent"
        ],
    entry_points={
        'console_scripts': [
            'fl_packagemake=auto_flat_lv.cli:cli_packagemake',
            'fl_datamake=auto_flat_lv.cli:cli_datamake',
            'fl_clean=auto_flat_lv.cli:cli_clean',
            'flat_lv=auto_flat_lv.cli:cli_flat_lv',
            'fl_remap=auto_flat_lv.cli:cli_remap',
            'seed_dbg=auto_flat_lv.cli:cli_seed_dbg',
            'flat_seed=auto_flat_lv.cli:cli_flat_seed',
        ],
    }

)
